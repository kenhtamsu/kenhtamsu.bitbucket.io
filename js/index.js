var config = {
    apiKey: "AIzaSyCnDAyF3tiWx1fs4W5yp8LwBcr-bH4jYBI",
    authDomain: "kenhtamsu-d81a0.firebaseapp.com",
    databaseURL: "https://kenhtamsu-d81a0.firebaseio.com",
    projectId: "kenhtamsu-d81a0",
    storageBucket: "kenhtamsu-d81a0.appspot.com",
    messagingSenderId: "265864948008"
  };
firebase.initializeApp(config);
firebase.auth().signInAnonymously();
firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        USER_ID = user.uid;
        if(!USE_LOCAL) {
            jQuery('#name').text(getDefaultName());
        }

        showJoinNotice();
    }
});

function getDefaultName() {
    return USER_ID.substring(0, 4).toLowerCase();
}

var icons = {};

jQuery(document).ready(function () {
    initKeysEvent();
    drawEmotiItems();
    drawAvatarItems();
    drawGiftItems();
    usePreviousInfo();
    initDataForIcon();
    setTitleForEmoti();
    initEventOnFileSelected();
});

function initDataForIcon() {
    icons[':D'] = 6;
    icons[':d'] = 6;
    icons[':/'] = 77;
    icons[':3'] = 3;
    icons[':))'] = 22;
    icons['><'] = 85;
    icons[':P'] = 23;
    icons[':p'] = 23;
    icons[':('] = 48;
    icons['=))'] = 22;
    icons[':s'] = 107;
    icons[':S'] = 107;
    icons[':(('] = 27;
    icons[':B'] = 80;
    icons[':b'] = 80;
    icons[':|'] = 32;
    icons[':v'] = 111;
    icons['<3'] = 113;
}

function writeGif(gif) {
    writeData('gif', gif);
}

function setTitleForEmoti() {
    var keys = Object.keys(icons);
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        var value = icons[key];
        var _class = '.item_' + value;
        jQuery(_class).attr('title', key);
    }
}

function getIcon(text) {
    var index = icons[text];
    if (index) {
        return '<img class="inner-icon" src="emoti/' + index + '.png"/>'
    } else {
        return text;
    }
}

var db_ref = firebase.database().ref('/');
db_ref.on('child_added', function (data) {
    var msg = data.val();
    appendMsg(msg);
});

var store_ref = firebase.storage().ref('/');

function upload(name, file) {
    updatePercent(0);
    showProgress();
    var task = store_ref.child(name).put(file);
    task.on('state_changed', function (snapshot) {
        var percent = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        updatePercent(Math.round(percent));
    }, function (error) {
        console.log(error);
    }, function () {
        task.snapshot.ref.getDownloadURL().then(function (downloadURL) {
            hideProgress();
            writeImage(downloadURL);
        });
    });
}

function writeImage(url) {
    writeData('image', url);
}

function usePreviousInfo() {
    var name = getLocal('name');
    if(name) {
        USE_LOCAL = true;
        jQuery('#name').text(name);
    }

    var avatar = getLocal('avatar');
    if(avatar) {
        jQuery('#avatar').attr('src', avatar);
    }
}

function appendMsg(msg) {
    var html = makeMsg(msg);
    if (html == null) {
        return;
    }
    jQuery('#sohbet').append(html);
    scrollDown();
    notifyMe(msg);
}

function getCurrentDate() {
    return moment().format(DATE_FORMAT);
}

function writeData(type, msg, notice) {
    var name = jQuery('#name').text();
    var avatar = jQuery('#avatar').attr('src');

    if(!notice) {
        checkAndSaveLocal('name', name);
        checkAndSaveLocal('avatar', avatar);
    }

    db_ref.push({
        type: type,
        message: msg,
        sendTime: getCurrentDate(),
        sender: USER_ID,
        name: name,
        avatar: avatar
    });
}

function checkAndSaveLocal(key, value) {
    var v = getLocal(key);
    if (v != value) {
        setLocal(key, value);

        if(v == null) {
            v = getDefaultName();
        }
        if (key == 'name') {
            sendRenameNotice(v, value);
        } else if (key == 'avatar') {
            sendAvatarChangeNotice();
        }
    }
}

function sendAvatarChangeNotice() {
    var name = jQuery('#name').text();
    var msg = '(' + name + ') changed avatar';
    writeNotice(msg);
}

function sendRenameNotice(oldName, newName) {
    var msg = '(' + oldName + ') renamed to (' + newName + ')';
    writeNotice(msg);
}

function processTextMsg(msg) {
    var items = msg.split(' ');
    for (var i = 0; i < items.length; i++) {
        var v = items[i];
        items[i] = getIcon(v);
    }
    var result = items.join(' ');
    if (items.length == 1) {
        result = result.replace('inner-icon', 'inner-icon-large');
    }

    return result;
}

function write(msg) {
    writeData('text', msg);
}

function writeEmoti(emoti) {
    writeData('emoti', emoti);
}

function writeNotice(notice) {
    writeData('notice', notice, true);
}

function send() {
    var msg = jQuery('#msg').val();
    if (msg.length == 0) {
        return;
    }
    write(msg);
    emptyInput();
}

function scrollDown() {
    jQuery("#sohbet").stop().animate({scrollTop: $("#sohbet")[0].scrollHeight}, 1000);
}

function emptyInput() {
    jQuery('#msg').val(null);
}

function makeMsg(msg) {
    if (msg.type == 'notice') {
        if (msg.sender == USER_ID) {
            return null;
        }
        return '<div class="join-notice" title="' + msg.sendTime + '">' + msg.message + '</div>';
    }

    var _class = msg.sender == USER_ID ? 'float-right' : 'float-left sohbet2';
    var _balon = msg.sender == USER_ID ? 'balon1' : 'balon2';
    var _msg_avatar = msg.sender == USER_ID ? 'msg-avatar-right' : 'msg-avatar-left';

    var avatar = '<img class="msg-avatar ' + _msg_avatar + ' ' + _class + ' " src="' + msg.avatar + '" />';

    var html = '<div class="' + _balon + ' p-2 m-0 position-relative" title="' + msg.sendTime + '" data-is="(' + msg.name + ') - ' + moment(msg.sendTime, DATE_FORMAT).format('h:mm a') + '">';
    html += avatar;
    if(msg.type == 'text') {
        var message = processTextMsg(msg.message);
        html += '<a class="' + _class + '"> ';
        html += message;
        html += '</a>';
    } else if(msg.type == 'emoti') {
        html += '<img class="msg-emoti ' + _class + '" src="' + msg.message + '"/>'
    } else if (msg.type == 'image') {
        html += '<img onclick="showFullImage(this)" class="msg-image ' + _class + '" src="' + msg.message + '"/>'
    } else if (msg.type == 'gif') {
        html += '<img class="msg-gif ' + _class + '" src="' + msg.message + '"/>'
    }
    html += '</div>';

    return html;
}

function drawGiftItems() {
    var target = jQuery('#gif-items');
    var index = 0;
    for (var i = 0; i < 42; i++) {
        var html = '<img onclick="onGifClick(this)" class="gif-item" src="gif/' + (++index) + '.gif" />'
        target.append(html);
    }
}

function initKeysEvent() {
    jQuery(document).keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            send();
        }
    });
}

function notifyMe(msg) {
    var sendTime = moment(msg.sendTime, DATE_FORMAT).toDate();
    if (sendTime < LAST_SEEN) {
        return;
    }

    LAST_SEEN = new Date();

    if (msg.sender == USER_ID) {
        return;
    }

    if (!Notification) {
        alert('Desktop notifications not available in your browser. Try Chromium.');
        return;
    }

    if (Notification.permission !== "granted") {
        Notification.requestPermission();
    } else {
        if (NOTICE) {
            NOTICE.close();
        }

        var notiMsg = (msg.type == 'text' || msg.type == 'notice') ? msg.message : 'Image';
        NOTICE = new Notification('Chat', {
            icon: 'https://hoangdieuctu.bitbucket.io/chat/img/ico.png',
            body: notiMsg
        });

        NOTICE.onclick = function (event) {
            event.preventDefault();
            window.open('https://hoangdieuctu.bitbucket.io/chat/index.html', '_blank');
        }
    }
}

function drawEmotiItems() {
    var target = jQuery('#emoti-items');
    var index = 0;
    for (var i = 0; i < 114; i++) {
        var html = '<img onclick="sendEmoji(this)" class="item item_' + (++index) + '" src="emoti/' + index + '.png" />'
        target.append(html);
    }
}

function drawAvatarItems() {
    var target = jQuery('#avatar-list');
    var index = 0;
    for (var i = 0; i < 72; i++) {
        var html = '<img onclick="onAvatarClick(this)" class="avatar-item" src="avatar/' + (++index) + '.png" />'
        target.append(html);
    }
}

function showHideEmotiItems() {
    var target = jQuery('#emoti-items');
    if (target.is(":visible")) {
        target.fadeOut();
    } else {
        jQuery('#gif-items').fadeOut();
        target.fadeIn(500);
    }
}

function showHideGifItems() {
    var target = jQuery('#gif-items');
    if (target.is(":visible")) {
        target.fadeOut();
    } else {
        jQuery('#emoti-items').fadeOut();
        target.fadeIn(500);
    }
}

function showHideAvatarItems() {
    var target = jQuery('#avatar-list');
    if (target.is(":visible")) {
        target.fadeOut();
    } else {
        target.fadeIn(500);
    }
}

function onMsgFocus() {
    jQuery('#emoti-items').fadeOut();
    jQuery('#avatar-list').fadeOut();
    jQuery('#gif-items').fadeOut();
}

function sendEmoji(ele) {
    var emoti = jQuery(ele).attr('src');
    jQuery('#emoti-items').hide();
    writeEmoti(emoti);
}

function onGifClick(ele) {
    var gif = jQuery(ele).attr('src');
    jQuery('#gif-items').hide();
    writeGif(gif);
}

function onAvatarClick(ele) {
    var avatar = jQuery(ele).attr('src');
    jQuery('#avatar').attr('src', avatar);
    jQuery('#avatar-list').fadeOut();
}

function getLocal(key) {
    return localStorage.getItem(key);
}

function setLocal(key, value) {
    return localStorage.setItem(key, value);
}

function showJoinNotice() {
    var name = jQuery('#name').text();
    var msg = '(' + name + ') joined';
    writeNotice(msg);
}

function showSelectFileDialog() {
    jQuery('#file').trigger('click');
}

function initEventOnFileSelected() {
    jQuery('#file').change(function (e) {
        var file = document.getElementById('file').files[0];
        if(file) {
            var name = getFileName(this.value);
            upload(name, file);
        }
    });
}

function getFileName(name) {
    var time = new Date().getTime();
    var extension = name.split('.').pop();
    return time + '.' + extension;
}

function showProgress() {
    jQuery('#progress').modal({backdrop: 'static', keyboard: false}, 'show');
}

function hideProgress() {
    jQuery('#progress').modal('hide');
}

function updatePercent(percent) {
    var target = jQuery('#percent');
    var value = percent + '%';
    target.css('width', value);
    target.text(value);
}

function showFullImage(ele) {
    var src = jQuery(ele).attr('src');
    jQuery('#img-modal-content').attr('src', src);
    jQuery('#img-modal').modal('show');
}